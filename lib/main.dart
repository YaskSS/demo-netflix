import 'package:flutter/material.dart';
import 'package:netflixflutter/screen/details_screen.dart';
import 'package:netflixflutter/scroll_layout.dart';

import 'mockData/mock_movies.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Netflix',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  PageController _pageController;

  @override
  void initState() {
    _pageController = PageController(initialPage: 1, viewportFraction: 0.8);
    super.initState();
  }

  AnimatedBuilder _movieSelector(int index) {
    return AnimatedBuilder(
      animation: _pageController,
      builder: (ctx, widget) {
        double val = 1;
        if (_pageController.position.haveDimensions) {
          val = _pageController.page - index;
          val = (1 - (val.abs() * 0.3) + 0.03).clamp(0.0, 1.0);
        }
        return Center(
          child: SizedBox(
            height: Curves.easeInOut.transform(val) * 270.0,
            width: Curves.easeInOut.transform(val) * 400.0,
            child: widget,
          ),
        );
      },
      child: GestureDetector(
        onTap: () {
          Navigator.push(context, MaterialPageRoute( builder: (_) => DetailsMovieScreen(movie: movies[index],)));
        },
        child: Stack(
          children: [
            Center(
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 15.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black54,
                          offset: Offset(0.0, 2.0),
                          blurRadius: 10.0)
                    ]),
                child: Center(
                  child: Hero(
                    tag: movies[index].imageUrl,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: Image(
                        image: AssetImage(movies[index].imageUrl),
                        height: 220.0,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              left: 30.0,
              bottom: 40.0,
              child: Container(
                width: 250.0,
                child: Text(
                  movies[index].title.toUpperCase(),
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Image(
            height: 60.0, image: AssetImage('assets/images/netflix_logo.png')),
        leading: IconButton(
          padding: EdgeInsets.only(left: 30.0),
          onPressed: () => print('Menu'),
          icon: Icon(Icons.menu),
          iconSize: 30.0,
          color: Colors.black,
        ),
        actions: <Widget>[
          IconButton(
            padding: EdgeInsets.only(right: 30.0),
            onPressed: () => print('Search'),
            icon: Icon(Icons.search),
            iconSize: 30.0,
            color: Colors.black,
          ),
        ],
      ),
      body: ListView(
        children: [
          Container(
            height: 250.0,
            width: double.infinity,
            child: PageView.builder(
              itemCount: movies.length,
              controller: _pageController,
              itemBuilder: (BuildContext ctx, int index) {
                return _movieSelector(index);
              },
            ),
          ),
          Container(
              height: 60.0,
              child: ListView.builder(
                padding: EdgeInsets.symmetric(horizontal: 30.0),
                scrollDirection: Axis.horizontal,
                itemCount: labels.length,
                itemBuilder: (ctx, index) {
                  return Container(
                    margin: EdgeInsets.all(10.0),
                    width: 160.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              Color(0xFFd45000),
                              Color(0xFFd11100),
                            ]),
                        boxShadow: [
                          BoxShadow(
                              color: Color(0xFFdE1F28),
                              offset: Offset(0.0, 2.0),
                              blurRadius: 6.0)
                        ]),
                    child: Center(
                      child: Text(
                        labels[index].toUpperCase(),
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold,
                            letterSpacing: 1.8),
                      ),
                    ),
                  );
                },
              )),
          SizedBox(
            height: 20.0,
          ),
          ScrollLayout(
            images: myList,
            title: 'Favorite movies',
            imageHeight: 250.0,
            imageWidth: 150.0,
          ),
          SizedBox(height: 12.0,),
          ScrollLayout(
            images: popular,
            title: 'Best movies',
            imageHeight: 250.0,
            imageWidth: 150.0,
          )
        ],
      ),
    );
  }
}
